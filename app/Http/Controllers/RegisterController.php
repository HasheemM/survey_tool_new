<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Sentinel;
use Activation;
use App\Models\User;
use Mail;

class RegisterController extends Controller
{
    public function register(){
        return view('auth.register');
    }

    public function post(Request $request) {
        $this->validate($request, [
            'first_name' => 'alpha|required|min:5|max:255',
            'last_name' => 'alpha|required|min:5|max:255',
            'email' => 'required|email|max:255',
            'password' => 'confirmed|required|min:5|max:255',
            'password_confirmation' => 'required|min:5|max:255',
        ]);
        $user = Sentinel::register($request->all());
        $activate = Activation::create($user);
        $role = Sentinel::findRoleBySlug('admin');
        $role->users()->attach($user);
        $this->sendMail($user, $activate->code);
        return redirect()->back()->with(['success' => 'Registration is successful cheack email to activate account']);
    }

    private function sendMail($user, $code){
        Mail::send('mail.adminactivation', [
            'user' => $user, 'code' => $code,
        ], function ($message) use ($user){
            $message->to($user->email);
            $message->subject("SOCITM Accout Activation");
        });
    }
}

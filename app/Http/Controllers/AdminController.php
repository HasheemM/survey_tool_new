<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    public $password;
    public function index(){
        return view('admin.index');
    }

    public function admins(){
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";
        $password = substr( str_shuffle( $chars ), 0, 12 );
        return view('admin.reviewers.create',compact('password'));
    }

    public function storeAdmin(Request $request) {
        $this->validate($request, [
            'first_name' => 'alpha|required|min:5|max:255',
            'last_name' => 'alpha|required|min:5|max:255',
            'email' => 'required|email|max:255',
        ]);
        $this->password = $request->input('password');
        $user = Sentinel::register($request->all());
        $activate = Activation::create($user);
        $role = Sentinel::findRoleBySlug('admin');
        $role->users()->attach($user);
        $this->sendMail($user, $activate->code);
        return redirect()->back()->with(['success' => 'Reviewer Created. Password and Activation code Sent to email']);
    }

    private function sendMail($user, $code){
        Mail::send('mail.activation', [
            'user' => $user, 'code' => $code, 'password' => $this->password,
        ], function ($message) use ($user){
            $message->to($user->email);
            $message->subject("SOCITM Accout Activation");
        });
    }
}

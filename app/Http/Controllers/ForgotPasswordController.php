<?php

namespace App\Http\Controllers;

use Reminder;
use Illuminate\Http\Request;
use App\Models\User;
use Sentinel;
use Mail;

class ForgotPasswordController extends Controller
{
    public function forgotPassword(){
        return view('auth.forgot-password');
    }

    public function post(Request $request){
        $this->validate($request, [
           'email' => 'required|email|max:255',
        ]);
        $user = User::whereEmail($request->email)->first();
        $sentinelUser = Sentinel::findById($user->id);
        if(count($user) == 0){
            return redirect()->back()->with(['error' => "The email doesn't exist"]);
        }else {
            $reminder = Reminder::exists($sentinelUser) ?: Reminder::create($sentinelUser);
            $this->sendMail($user, $reminder->code);
            return redirect()->back()->with(['success' => 'Reset code was sent to your email']);
        }
    }

    public function reset($email, $resetCode){
        $user = User::byEmail($email);
        $sentinelUser = Sentinel::findById($user->id);
        if(count($user) == 0)
            abort('404');

        if($reminder = Reminder::exists($sentinelUser)){
            if($resetCode == $reminder->code){
               return view('auth.reset-password');
            }else{
                return redirect('/');
            }
        }else {
            return redirect('/');
        }
    }

    public function postReset(Request $request, $email, $resetCode){
        $this->validate($request, ['password' => 'confirmed|required|max:255',
        'password_confirmation' => 'required|same:password']);

        $user = User::byEmail($email);
        $sentinelUser = Sentinel::findById($user->id);
        if(count($user) == 0)
            abort('404');
        if($reminder = Reminder::exists($sentinelUser)){
            if($resetCode == $request->code){
                Reminder::complete($sentinelUser, $resetCode, $request->password);
                return redirect('/login')->with(['success' => 'Login with your new Password']);
            }else{
                return redirect('/');
            }
        }else {
            return redirect('/');
        }

    }

    private function sendMail($user, $code){
        Mail::send('mail.forgotpassword',['user' => $user,
        'code' => $code], function ($message) use ($user){
           $message->to($user->email);
           $message->subject("SOCITM Password Reset");
        });
    }
}

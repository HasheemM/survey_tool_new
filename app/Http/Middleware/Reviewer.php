<?php

namespace App\Http\Middleware;

use Closure;
use Sentinel;

class Reviewer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public $user_id;

    public function handle($request, Closure $next)
    {
        if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug == 'reviewer') {
            $user = Sentinel::check();
            return $next($request);
        }else{
            return redirect('/');
        }
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Survey extends Model
{
    protected $table = 'surveys';
    protected $fillable = [
        'name',
        'task_name',
        'council_id',
        'service_area_id',
        'mobile',
        'multipart',
        'notes',
        'year',
        'deadline',
        'status_id',
    ];

    public function reviewer(){
        return $this->belongsTo('App\Models\Reviewer', 'reviewer_id');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouncilsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('councils', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('full_name');
            $table->integer('insight_subscriber');
            $table->integer('counciltype_id')->unsigned();
            $table->integer('ons_areacode_id');
            $table->integer('region_id')->unsigned();
            $table->string('domain_name');
            $table->integer('ref_2015');
            $table->integer('ref_2016');
            $table->string('area');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('councils');
    }
}

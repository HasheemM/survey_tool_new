@extends('layouts.master')
@section('content')
    <div class="row">
        <table class="table table-responsive table-stripped">
            <thead>
            <tr>
                <th>Survey</th>
                <th class="col-sm-1">Deadline</th>
                <th class="col-sm-1">Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($surveys as $survey)
                <tr>
                    <td>{{ $survey->name }}</td>
                    <td>{{ $survey->deadline }}</td>
                    <td><a href="#">Manage</a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
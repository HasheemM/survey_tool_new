@extends('layouts.master')
@section('content')
    <div class="row">
        <form action="/updatesurvey" method="post">
            @include('admin.surveys.form')
        </form>
    </div>
@endsection
@extends('layouts.master')
@section('content')
    <div class="row">
        <table class="table table-striped table-responsive">
            <thead>
            <tr>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>Surveys</th>
                <th></th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr>
                    <td>{{ $user->first_name }}</td>
                    <td>{{ $user->last_name }}</td>
                    <td>{{ $user->email }}</td>
                    <td><a href="{{ url('/reviewersurveys', $user->id) }}">View Surveys</a></td>
                    <td><a href="{{ url('/edit', $user->id)}}"><span class="btn btn-warning">Edit</span></a></td>
                    <td>{!! Form::open(['method' => 'DELETE', 'url' => ['/delete', $user->id]]) !!}
                        {!! Form::submit('Delete', array('class' => 'btn btn-danger')) !!}
                        {!! Form::close() !!}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="row"><a href="/createreviewer"><button class="btn btn-success col-md-offset-10">Add Reviewer</button></a></div>
    </div>
@endsection
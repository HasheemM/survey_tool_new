@extends('layouts.master');
@section('content')
<h3>Service Area Surveys</h3>
<hr>
<div class="row">
    <form class="form-inline" role="form" method="get" action="">
        <fieldset>
            <legend><small>Search for Survey</small></legend>
            <div class="form-group" style="margin-top: 10px;">
                <input type="text" name="name" id="name" class="form-control" placeholder="Survey Name">
            </div>&nbsp;
            <button type="submit" class="btn btn-default" name="search" style="margin-top: 10px;">Search</button>
        </fieldset>
    </form>
    <hr>
    <table class="table table-striped table-responsive">
        <thead>
        <tr>
            <th class="col-sm-6">Survey</th>
            <th>Status</th>
            <th class="col-sm-3">Action</th>
            <th class="col-sm-1">Year</th>
            <th class="col-sm-1">Deadline</th>
        </tr>
        </thead>
        <tbody>
        @foreach($all_surveys as $survey)
            <tr>
                <td>{{ $survey->name }}</td>
                <td></td>
                <td>
                    <a href="{{ url('/editsurvey', $survey->id) }}">Edit</a> /
                    <a href="#">View Results</a>
                </td>
                <td>{{ $survey->year }}</td>
                <td>2{{ $survey->deadline }}</td>
            </tr>
          @endforeach
        </tbody>
    </table>

    <hr>
    <a href="/create" class="btn btn-success">
        Create New Service Area Survey
    </a>

    <hr>
    <h3>Mobile Surveys</h3>
    <hr>
    <table class="table table-striped table-responsive">
        <thead>
        <tr>
            <th class="col-sm-6">Survey</th>
            <th>Status</th>
            <th class="col-sm-3">Action</th>
            <th class="col-sm-1">Year</th>
            <th class="col-sm-1">Deadline</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>Survey</td>
            <td>0</td>
            <td>
                <a href="#">Edit</a> /
                <a href="#">View Results</a>
            </td>
            <td>2017</td>
            <td>20/6/2017</td>
        </tr>
        </tbody>
    </table>

    <hr>
    <a href="#" class="btn btn-success">
        Create New Mobile Survey
    </a>

    <hr>
    <h3>Nav/search/A-Z Surveys</h3>
    <hr>
    <table class="table table-striped table-responsive">
        <thead>
        <tr>
            <th class="col-sm-6">Survey</th>
            <th>Status</th>
            <th class="col-sm-3">Action</th>
            <th class="col-sm-1">Year</th>
            <th class="col-sm-1">Deadline</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>Survey</td>
            <td>0</td>
            <td>
                <a href="#">Edit</a> /
                <a href="#">View Results</a>
            </td>
            <td>2017</td>
            <td>20/6/2017</td>
        </tr>
        </tbody>
    </table>
    <hr>
    <a href="#" class="btn btn-success">
        Create New Mobile Survey
    </a>
</div>
@endsection
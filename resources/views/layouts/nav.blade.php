<div class="header clearfix">
    <a href="/">
        <img width="120px" src="img/logo_0.gif" alt="SOCITM">
    </a>
    <h3 class="text-muted">Better<strong>Connected</strong>
        <small>Survey Tool</small></h3>
    <nav>

        <ul class="nav nav-pills">
            @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug == 'admin')
                    <li role="presentation"><a href="/surveys">Manage Surveys</a></li>
                    <li role="presentation"><a href="/councils">Manage Councils</a></li>
                    <li role="presentation"><a href="/reviewers">Manage Reviewers</a></li>
            @endif
            <div class="pull-right">
            @if(Sentinel::check())
                <li role="presentation">{{ Sentinel::getUser()->first_name }}&nbsp; &nbsp;</li>
                <li role="presentation">
                    <form action="/logout" method="post" id="logout-form">
                        {{ csrf_field() }}
                        <a href="#" onclick="document.getElementById('logout-form').submit()">Logout</a>
                    </form>
                </li>
            @else
                <li role="presentation"><a href="/login">Login</a></li>
                <li role="presentation"><a href="/register">Register</a></li>
            @endif
            </div>
        </ul>
    </nav>
</div>
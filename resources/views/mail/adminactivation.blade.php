<h2>Hello {{ $user->first_name }}</h2>
<p>
    Please click the link to activate your SOCITM survey account.
    <a href="http://survey.dev/activation/{{ $user->email }}/{{ $code }}">Activate Account</a>
</p>
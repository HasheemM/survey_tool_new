<h2>Hello {{ $user->first_name }}</h2>
<p>
    Please use the password below and click the link to activate your SOCITM survey account.
    Password : <strong>{{ $password }}</strong> &nbsp;
    <a href="http://survey.dev/activation/{{ $user->email }}/{{ $code }}">Activate Account</a>
</p>